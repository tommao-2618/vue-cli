import Vue from "vue";
import { Utils } from "@/common/utils";
import { GlobalData } from "@/common/global-data";

export default Vue.extend({
  data() {
    return {
      isCollapse: false,
      adminMenus: null,
      currentAdminUser: null,
      //组件数据
      dialogVisible: false,
      dialogUptPassword: false,
      loading: false,
      uptPasswordData: {
        oldPassword: null,
        newPassword: null,
      },
    };
  },
  methods: {
    /**
     * 菜单缩放
     */
    toggleMenu() {
      this.isCollapse = !this.isCollapse;
    },
    /**
     * 去主页
     */
    goHome() {
      if (this.$route.path != "/home") {
        this.$router.push("/home");
      }
    },
    /**
     * 浏览器刷新
     */
    renovate() {
      //浏览器刷新
      location.reload();
    },
    /**
     * 退出登录
     */
    signOut() {
      this.$confirm("确认退出?", "提示", {})
        .then(() => {
          sessionStorage.removeItem("CURRENT_USER_TOKEN");
          this.$router.replace({ path: "/" });
        })
        .catch(() => {});
    },

    /**
     * 打开更新密码弹窗
     */
    openUptPassword() {
      this.dialogUptPassword = true;
    },
    /**
     * 更新密码
     */
    uptPassword() {
      let url: string =
        GlobalData.Str.APIURL + "AdminUser/UptAdminUserPassword";
      let params = {
        BId: this.currentAdminUser.bId,
        OldPassword: this.uptPasswordData.oldPassword,
        NewPassword: this.uptPasswordData.newPassword,
      };
      Utils.HttpHelper.Post(url, params).then((res: any) => {
        Utils.EUIHelper.MessageTips("成功", 1);
        this.dialogUptPassword = false;
        Utils.DataHelper.ClearObj(this.uptPasswordData);
        sessionStorage.removeItem("CURRENT_USER_TOKEN");
        this.$router.replace({ path: "/" });
      });
    },
    /**
     *
     */
    handleUptPasswordCancel() {
      this.dialogUptPassword = false;
      Utils.DataHelper.ClearObj(this.uptPasswordData);
    },
    /**
     * 异步加载11
     */
    async initAsync() {
      this.currentAdminUser = await GlobalData.Obj.GetCurrentAdminUser();
      if (this.currentAdminUser.type != 0) {
        this.adminMenus = this.currentAdminUser.router;
      } else {
        this.adminMenus = this.$router.options.routes;
      }
    },
  },
  mounted() {
    this.initAsync();
  },
});
