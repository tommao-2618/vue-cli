import Vue from "vue";
import { Utils } from "@/common/utils";
import { GlobalData } from "@/common/global-data";
import roleAuth from "./roleAuth.vue";
export default Vue.extend({
  components: { roleAuth },
  data() {
    return {
      searchParams: {
        pageIndex: 1,
      },
      modifyParams: {
        role: null,
        type: null,
      },
      roleList: null,
      totalCount: 0,
      pageSize: 0,
      pageIndex: 0,
      loading: false,
      dialogVisible: false,
      roleBId: "",
      dialogAdd: false,
    };
  },
  methods: {
    /**
     * 获取角色列表
     */
    getRoleList() {
      this.loading = true;
      let url: string = GlobalData.Str.APIURL + "AdminUser/GetUserRoleToPage";
      let params = {
        pageIndex: this.searchParams.pageIndex,
      };
      Utils.HttpHelper.Get(url, params).then((res: any) => {
        this.roleList = res.items;
        this.totalCount = res.totalCount;
        this.pageSize = res.pageSize;
        this.pageIndex = res.pageIndex;
        this.searchParams.pageIndex = 1;
        this.loading = false;
      });
    },

    handleAuthClick(row: any) {
      this.dialogVisible = true;
      this.roleBId = row.bId;
      // this.$router.push({ path: "/role-auth", query: { roleBId: row.bId } });
    },
    handleDelete(row: any) {
      this.$confirm("确定删除此角色？", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning",
      })
        .then(() => {
          let url: string = GlobalData.Str.APIURL + "AdminUser/DelUserRole";
          let params = {
            bId: row.bId,
          };
          Utils.HttpHelper.Post(url, params).then((res: any) => {
            Utils.EUIHelper.MessageTips("成功", 1);
            this.getRoleList();
          });
        })
        .catch(() => {});
    },
    /**
     * 处理分页
     * @param page 页码
     */
    handleCurrentChange(page: any) {
      this.searchParams.pageIndex = page;
      this.getRoleList();
    },
    handleAdd() {
      this.dialogAdd = true;
    },
    handleAddCancel() {
      this.dialogAdd = false;
    },
    addUserRole() {
      this.loading = true;
      let url: string = GlobalData.Str.APIURL + "AdminUser/AddUserRole";
      let params = {
        role: this.modifyParams.role,
        type: this.modifyParams.type,
      };
      Utils.HttpHelper.Post(url, params).then((res: any) => {
        this.loading = false;
        Utils.EUIHelper.MessageTips("添加成功", 1);
        this.dialogAdd = false;
        Utils.DataHelper.ClearObj(this.modifyParams);
        this.getRoleList();
      });
    },
  },
  mounted() {
    this.getRoleList();
  },
});
