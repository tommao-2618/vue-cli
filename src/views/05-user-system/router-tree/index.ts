import Vue from "vue";
import { Utils } from "@/common/utils";
import { GlobalData } from "@/common/global-data";
import IconSelect from "@/components/IconSelect/index.vue";
export default Vue.extend({
  components: { IconSelect },
  data() {
    return {
      treeProps: {
        children: "children",
        label: "name",
      },
      loading: false,
      refreshTable: true, //重新渲染表格状态
      isExpandAll: false, //是否展开，默认全部折叠
      expandRrowKeys: [], //展开行的 keys 数组
      treeData: [{ children: [] }],
      dialogVisible: false,
      isAdd: false,
      modifyParams: {
        id: null,
        name: null,
        path: null,
        icon: null,
        display: null,
        parentId: null,
        type: null,
        api: null,
      },
    };
  },
  mounted() {
    this.getRouterTree();
  },

  methods: {
    getRouterTree() {
      let url: string = GlobalData.Str.APIURL + "AdminUser/GetRouterTree";
      let params = {};
      this.loading = true;
      Utils.HttpHelper.Get(url, params).then((res: any) => {
        this.treeData = res;
        let openArr = res.map((item) => {
          return item.id.toString();
        });
        this.expandRrowKeys = openArr;
        this.loading = false;
        console.log("====res:", res, this.expandRrowKeys);
      });
    },

    /** 展开/折叠操作 */
    toggleExpandAll() {
      this.refreshTable = false;
      this.isExpandAll = !this.isExpandAll;
      this.$nextTick(() => {
        this.refreshTable = true;
      });
    },

    // 添加
    appendTreeNode(data) {
      console.log("---gagaga", data);
      this.dialogVisible = true;
      this.isAdd = true;
      this.modifyParams.parentId = data.id;
      this.modifyParams.type = data.type + 1;
    },

    //  修改
    updateTreeNode(data) {
      console.log(data);
      this.dialogVisible = true;
      this.isAdd = false;
      this.modifyParams.id = data.id;
      this.modifyParams.name = data.name;
      this.modifyParams.path = data.path;
      this.modifyParams.icon = data.icon;
      this.modifyParams.display = data.display == 1 ? true : false;
      this.modifyParams.api = data.api;
    },

    // 删除
    removeTreeNode(data) {
      let url: string = GlobalData.Str.APIURL + "AdminUser/DelRoleRouter";
      let params = { id: data.id };
      Utils.HttpHelper.Get(url, params).then((res: any) => {
        Utils.EUIHelper.MessageTips("操作成功", 1);
        this.getRouterTree();
      });
    },

    // 确定
    modifyTree() {
      console.log(this.modifyParams);
      let url: string;
      let params: object;
      if (this.isAdd) {
        url = GlobalData.Str.APIURL + "AdminUser/AddRoleRouter";
        params = {
          name: this.modifyParams.name,
          path: this.modifyParams.path,
          icon: this.modifyParams.icon,
          display: this.modifyParams.display ? 1 : 0,
          parentId: this.modifyParams.parentId,
          type: this.modifyParams.type,
          api: this.modifyParams.api,
        };
      } else {
        url = GlobalData.Str.APIURL + "AdminUser/UpdateRoleRouter";
        params = {
          id: this.modifyParams.id,
          name: this.modifyParams.name,
          path: this.modifyParams.path,
          icon: this.modifyParams.icon,
          display: this.modifyParams.display ? 1 : 0,
          api: this.modifyParams.api,
        };
      }
      Utils.HttpHelper.Post(url, params).then((res: any) => {
        this.dialogVisible = false;
        Utils.EUIHelper.MessageTips("操作成功", 1);
        this.getRouterTree();
        this.modifyParams = Utils.DataHelper.ClearObj(this.modifyParams);
      });
    },

    // 取消
    handleCancel() {
      this.modifyParams = Utils.DataHelper.ClearObj(this.modifyParams);
      this.dialogVisible = false;
    },

    // 选择图标
    selectIcon(value) {
      console.log("-----图标值", value);
    },
  },
});
