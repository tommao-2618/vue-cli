import Vue from "vue";
import { Utils } from "@/common/utils";
import { GlobalData } from "@/common/global-data";

export default Vue.extend({
  data() {
    return {
      searchParams: {
        name: null,
        pageIndex: 1,
      },
      adminUserList: null,
      roleList: null,
      totalCount: 0,
      pageSize: 0,
      pageIndex: 0,
      loading: false,
      modifyParams: {
        bId: null,
        account: null,
        password: null,
        name: null,
        avatar: null,
        role: null,
      },
      dialogAdd: false,
      dialogEdit: false,
    };
  },
  methods: {
    /**
     * 获取管理员列表
     */
    getAdminUserList() {
      this.loading = true;
      let url: string = GlobalData.Str.APIURL + "AdminUser/GetAdminUserToPage";
      let params = {
        name: this.searchParams.name,
        pageIndex: this.searchParams.pageIndex,
      };
      Utils.HttpHelper.Get(url, params).then((res: any) => {
        this.adminUserList = res.items;
        this.totalCount = res.totalCount;
        this.pageSize = res.pageSize;
        this.pageIndex = res.pageIndex;
        this.searchParams.pageIndex = 1;
        this.loading = false;
      });
    },
    getRoleList() {
      let url: string = GlobalData.Str.APIURL + "AdminUser/GetUserRoleList";
      let params = {};
      Utils.HttpHelper.Get(url, params).then((res: any) => {
        this.roleList = res;
      });
    },
    /**
     * 处理分页
     * @param page 页码
     */
    handleCurrentChange(page: any) {
      this.searchParams.pageIndex = page;
      this.getAdminUserList();
    },
    handleAddCancel() {
      this.dialogAdd = false;
    },
    handleEditCancel() {
      this.dialogEdit = false;
    },
    handleAdd() {
      this.dialogAdd = true;
      Utils.DataHelper.ClearObj(this.modifyParams);
      this.getRoleList();
    },
    addAdminUser() {
      let url: string = GlobalData.Str.APIURL + "AdminUser/AddAdminUser";
      let params = {
        account: this.modifyParams.account,
        password: this.modifyParams.password,
        avatar: this.modifyParams.avatar,
        name: this.modifyParams.name,
        roleBId: this.modifyParams.role,
      };
      Utils.HttpHelper.Post(url, params).then((res: any) => {
        Utils.EUIHelper.MessageTips("成功", 1);
        this.dialogAdd = false;
        this.getAdminUserList();
      });
    },
    handleDelete(row: any) {
      this.$confirm("确定删除此管理员？", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning",
      })
        .then(() => {
          let url: string = GlobalData.Str.APIURL + "AdminUser/DelAdminUser";
          let params = {
            bId: row.bId,
          };
          Utils.HttpHelper.Post(url, params).then((res: any) => {
            Utils.EUIHelper.MessageTips("成功", 1);
            this.getAdminUserList();
          });
        })
        .catch(() => {});
    },
    handleEdit(row: any) {
      this.dialogEdit = true;
      Utils.DataHelper.ClearObj(this.modifyParams);
      this.modifyParams.role = row.roleBId;
      this.modifyParams.bId = row.bId;
      this.getRoleList();
    },
    uptAdminUserRole() {
      console.log(this.modifyParams);
      let params = {
        bId: this.modifyParams.bId,
        roleBId: this.modifyParams.role,
      };
      let url: string = GlobalData.Str.APIURL + "AdminUser/UptAdminUserRole";
      Utils.HttpHelper.Post(url, params).then((res: any) => {
        Utils.EUIHelper.MessageTips("成功", 1);
        this.dialogEdit = false;
        this.getAdminUserList();
      });
    },
  },
  mounted() {
    this.getAdminUserList();
  },
});
