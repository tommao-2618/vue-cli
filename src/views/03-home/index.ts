import Vue from "vue";
import { Utils } from "@/common/utils";
import { GlobalData } from "@/common/global-data";

export default Vue.extend({
  data() {
    return {
      data: [],
    };
  },
  methods: {
    getEchartData() {
      const chart = this.$refs.chart;
      if (chart) {
        const myChart = this.$echarts.init(chart);
        this.loading = true;
        let url: string = GlobalData.Str.APIURL + "Home/GetLogRecordStatTrend";
        let params = {
          type: 0,
          queryDateType: 4,
        };
        Utils.HttpHelper.Get(url, params).then((res: any) => {
          const option = {
            title: { text: "" },
            color: ["#409EFF"],
            xAxis: {
              type: "category",
              data: res.xAxis,
            },
            yAxis: {
              type: "value",
              name: "日志量",
              min: 0,
              position: "left",
            },
            series: [
              {
                name: "日志量",
                data: res.series.logList,
                type: "bar",
              },
            ],
          };
          myChart.setOption(option);
        });
        window.addEventListener("resize", function() {
          myChart.resize();
        });
      }
      this.$on("hook:destroyed", () => {
        window.removeEventListener("resize", function() {
          chart.resize();
        });
      });
    },
  },
  mounted() {
    this.getEchartData();
  },
});
