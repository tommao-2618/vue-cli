import Vue from "vue";
import { Utils } from "@/common/utils";
import { GlobalData } from "@/common/global-data";

export default Vue.extend({
  data() {
    return {};
  },
  methods: {
    ajaxModel() {
      this.loading = true;
      let url: string = GlobalData.Str.APIURL + "Open/Index";
      let params = {};
      Utils.HttpHelper.Post(url, params).then((res: any) => {});
    },
  },
  mounted() {
    this.ajaxModel();
  },
});
