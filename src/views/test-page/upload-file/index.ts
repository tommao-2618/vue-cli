import Vue from "vue";
import { Utils } from '@/common/utils';
import { GlobalData } from "@/common/global-data";

export default Vue.extend({
    data() {
        return {
            uploadUrl: GlobalData.Str.APIURL + "Open/UploadFile",
            uploadFileList: []
        };
    },
    methods: {
        handleRemove(file, fileList) {
            console.log(file, fileList);
        },
        handlePreview(file) {
            console.log(file);
        },
        handleExceed(files, fileList) {
            this.$message.warning(`当前限制选择 3 个文件，本次选择了 ${files.length} 个文件，共选择了 ${files.length + fileList.length} 个文件`);
        },
        beforeRemove(file, fileList) {
            return this.$confirm(`确定移除 ${file.name}？`);
        },
        onSuccess(response, file, fileList) {
            var a = response;
            var b = file;
            var c = fileList;

            console.log(response.data.webUrl);
            if (response.status == 200) {
                this.uploadFileList.push({ name: response.data.fileName, url: response.data.webUrl });
            }
        },
        exportExcel() {
            let url: string = GlobalData.Str.APIURL + "Open/ExportExcelToStream";
            Utils.HttpHelper.Get(url, {}).then((res: any) => {
                Utils.ExcelHelper.DownloadFileByByte(res, '测试.xlsx')   
            });
        }
    },
    mounted() { }
});