import Vue from "vue";
import { Utils } from "@/common/utils";
import { GlobalData } from "@/common/global-data";

export default Vue.extend({
  data() {
    return {
      loading: false,
      pwdType: "password",
      formParams: {
        account: "15768196734",
        password: "666666",
      },
      formRules: {
        account: [{ required: true, message: "请输入账号！", trigger: "blur" }],
        password: [
          { required: true, message: "请输入密码！", trigger: "blur" },
        ],
      },
    };
  },
  methods: {
    /**
     * 登录提交
     */
    signIn() {
      
      // valid不能为any类型,否则表单验证会失效。
      this.$refs.formParams.validate(async (valid: string | number) => {
        if (valid) {
          this.loading = true;
          let url = GlobalData.Str.APIURL + "Open/Login";
          let params = {
            account: this.formParams.account,
            password: this.formParams.password,
          };
          await Utils.HttpHelper.Get(url, params, false).then((res: any) => {
            Utils.EUIHelper.MessageTips("登录成功！", 1);
            //将管理员信息保存在sessionStorage
            sessionStorage.setItem("CURRENT_USER_TOKEN", JSON.stringify(res));
            this.$router.push({ path: "/home" });
          });
        } else {
          return false;
        }
      });
    },
    /**
     * 密码输入框眼睛点击事件
     */
    showPassword() {
      // 绑定input输入框的type
      if (this.pwdType === "password") {
        this.pwdType = "";
      } else {
        this.pwdType = "password";
      }
    },
  },
  mounted() {},
});
