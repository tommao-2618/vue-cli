import Vue from "vue";
import { Utils } from "@/common/utils";
import { GlobalData } from "@/common/global-data";

export default Vue.extend({
  data() {
    return {
      searchParams: {
        traceId: null,
        logger: null,
        message: null,
        exception: null,
        url: null,
        startTime: null,
        endTime: null,
        pageIndex: 1,
      },
      logList: null,
      totalCount: 0,
      pageSize: 0,
      pageIndex: 0,
      loading: false,
      isExport: false,
    };
  },
  methods: {
    /**
     * 获取异常日志列表
     */
    getLogList() {
      this.loading = true;
      let url: string = GlobalData.Str.APIURL + "Home/GetLogRecordToPage";
      let params = {
        traceId: this.searchParams.traceId,
        logger: this.searchParams.logger,
        message: this.searchParams.message,
        exception: this.searchParams.exception,
        netRequestUrl: this.searchParams.url,
        logStartDate: this.searchParams.startTime,
        logEndDate: this.searchParams.endTime,
        pageIndex: this.searchParams.pageIndex,
        isExport: this.isExport,
      };
      Utils.HttpHelper.Get(url, params).then((res: any) => {
        this.logList = res.items;
        this.totalCount = res.totalCount;
        this.pageSize = res.pageSize;
        this.pageIndex = res.pageIndex;
        this.searchParams.pageIndex = 1;
        this.loading = false;
        if (this.isExport) {
          Utils.ExcelHelper.DownloadFileByByte(
            res.exportExcelByte,
            "日志列表.xlsx"
          );
          this.isExport = false;
        }
      });
    },
    /**
     * 导出日志列表
     */
    exportLogList() {
      this.isExport = true;
      this.getLogList();
    },
    /**
     * 处理分页
     * @param page 页码
     */
    handleCurrentChange(page: any) {
      this.searchParams.pageIndex = page;
      this.getLogList();
    },
  },
  mounted() {
    this.getLogList();
  },
});
