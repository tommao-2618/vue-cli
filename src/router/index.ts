import Vue from "vue";
import VueRouter from "vue-router";
import Login from "@/views/01-login/index.vue";
import Main from "@/views/02-main/index.vue";
import Home from "@/views/03-home/index.vue";
import ErrorPage from "@/views/error-page/index.vue";
import Log from "@/views/04-system-info/log/index.vue";
import Role from "@/views/05-user-system/role/index.vue";
import RouterTree from "@/views/05-user-system/router-tree/index.vue";
import AdminUser from "@/views/05-user-system/admin-user/index.vue";
import UploadFile from "@/views/test-page/upload-file/index.vue";
import DataTest from "@/views/test-page/data-test/index.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "登录",
    component: Login,
  },
  {
    path: "/main",
    name: "首页系统",
    display: false,
    icon: "el-icon-s-home",
    component: Main,
    children: [
      { path: "/home", name: "首页", display: false, component: Home },
    ],
  },
  {
    path: "/main",
    name: "系统信息",
    display: true,
    icon: "el-icon-s-tools",
    component: Main,
    children: [
      { path: "/log", name: "系统日志", display: true, component: Log },
    ],
  },
  {
    path: "/main",
    name: "用户系统",
    display: true,
    icon: "el-icon-s-custom",
    component: Main,
    children: [
      { path: "/admin-user", name: "管理员列表", display: true, component: AdminUser },
      { path: "/role", name: "角色列表", display: true, component: Role },
      { path: "/router-tree", name: "权限配置", display: true, component: RouterTree },
    ],
  },
  {
    path: "/main",
    name: "测试系统",
    display: true,
    icon: "el-icon-info",
    component: Main,
    children: [
      { path: "/upload-file", name: "文件上传", display: true, component: UploadFile },
      { path: "/data-test", name: "数据测试", display: true, component: DataTest },
    ],
  },
  {
    //*是路由未找到时的跳转，须放到路由配置的最后面
    path: "*",
    name: "404页",
    component: ErrorPage,
  },
];

const router = new VueRouter({
  // 打包环境下用hash模式，不然会直接跳到*
  mode: "hash",
  base: process.env.BASE_URL,
  routes,
});

export default router;
