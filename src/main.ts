import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import "@/styles/global.scss";
import axios from "axios";
import VueAxios from "vue-axios";
import echarts from 'echarts'

Vue.config.productionTip = false;
Vue.use(ElementUI);
Vue.use(VueAxios, axios);
Vue.prototype.$echarts = echarts;

//全局导航守卫，当用户未登录时请求页面，会跳转到登录页
router.beforeEach((to, from, next) => {
  let CURRENT_USER = sessionStorage.getItem("CURRENT_USER_TOKEN");
  if (CURRENT_USER == null && to.path !== "/") {
    next({path: "/",});
  } else {
    next();
  }
});

new Vue({
  router,
  store,

  render: (h) => h(App),
}).$mount("#app");
