import Vue from "vue";
import axios from "axios";
import store from "@/store";
import { GlobalData } from "./global-data";

export namespace Utils {
  /**
   * ElementUI组件封装
   */
  export class EUIHelper {
    /**
     * 消息提示
     * @param msg
     * @param type 1=成功，2=警告，3=错误
     */
    static MessageTips(msg: string, type: number) {
      if (msg == null) {
        msg = "系统异常！";
      }
      switch (type) {
        case 1:
          Vue.prototype.$message({
            showClose: true,
            message: msg,
            type: "success",
          });
          break;
        case 2:
          Vue.prototype.$message({
            showClose: true,
            message: msg,
            type: "warning",
          });
          break;
        case 3:
          Vue.prototype.$message({
            showClose: true,
            message: msg,
            type: "error",
          });
          break;
        default:
          Vue.prototype.$message({
            showClose: true,
            message: msg,
          });
      }
    }
    static Validator(params: any) {
      Vue.prototype.$refs[params].validate((valid: string | number) => {
        if (valid) {
        } else {
          return false;
        }
      });
    }
  }
  /**
   * 处理请求相关
   */
  export class HttpHelper {
    /**
     * Post请求
     * @param apiUrl
     * @param params
     * @param addToken
     * @returns
     */
    static async Post(
      apiUrl: string,
      params: any,
      addToken: boolean = true
    ): Promise<any> {
      return new Promise((resolve, reject) => {
        var headers = null;
        if (addToken) {
          headers = {
            Authorization: GlobalData.Obj.CURRENT_USER_TOKEN,
          };
        }
        axios
          .post(apiUrl, params, { headers })
          .then((res) => {
            if (res.data.status == 200 && res.data.data.code == 1) {
              resolve(res.data.data.data);
            } else {
              EUIHelper.MessageTips(res.data.data.msg, 3);
            }
          })
          .catch((err) => {
            EUIHelper.MessageTips("系统异常！请找管理员", 3);
          });
      });
    }
    /**
     * Get请求
     * @param apiUrl
     * @param params
     * @param addToken
     * @returns
     */
    static async Get(
      apiUrl: string,
      params: any,
      addToken: boolean = true
    ): Promise<any> {
      return new Promise((resolve, reject) => {
        var headers = null;
        if (addToken) {
          headers = {
            Authorization: GlobalData.Obj.CURRENT_USER_TOKEN,
          };
        }
        axios
          .get(apiUrl, { params, headers })
          .then((res) => {
            if (res.data.status == 200 && res.data.data.code == 1) {
              resolve(res.data.data.data);
            } else {
              EUIHelper.MessageTips(res.data.data.msg, 3);
            }
          })
          .catch((err) => {
            reject(`front-end-error:${err}`);
            EUIHelper.MessageTips(err, 3);
          });
      });
    }
  }
  /**
   * 处理系统枚举
   */
  export class EnumHelper {
    /**
     * 获取下拉列表value对应的label。
     * value对应系统枚举值
     * @param ary
     * @param index
     */
    static ConvertValueToLabel(ary: any, index: number): string {
      for (let item of ary) {
        if (item.value == index) {
          return item.label;
        }
      }
    }
  }
  /**
   * 验证信息
   */
  export class ValidHelper {
    /**
     * 验证密码只能由数字和字母组成
     * @param param
     */
    static ValidPassword(param: string) {
      let regex = /^[0-9a-zA-Z]*$/;
      return regex.test(param);
    }
  }
  export class ExcelHelper {
    static DownloadFile(blob, fileName) {
      const link = document.createElement("a");
      link.href = window.URL.createObjectURL(blob);
      link.download = fileName;
      // 此写法兼容可火狐浏览器
      document.body.appendChild(link);
      var evt = new MouseEvent("click")
      link.dispatchEvent(evt);
      document.body.removeChild(link);
    }
    static BuildBlobByByte(data) {
      const raw = window.atob(data);
      const rawLength = raw.length;
      const uInt8Array = new Uint8Array(rawLength);
      for (let i = 0; i < rawLength; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
      }
      return new Blob([uInt8Array]);
    }
    static DownloadFileByByte(data, fileName) {
      const blob = this.BuildBlobByByte(data);
      this.DownloadFile(blob, fileName);
    }
  }

  export class DataHelper {
    /**
     * 清空对象值
     * @param obj 
     * @returns 
     */
    static ClearObj(obj: object) {
      for (let key in obj) {
        obj[key] = "";
      }
      return obj;
    }

    /**
     * list转为树
     * @param list 数据源
     * @param rootId 根id
     * @returns 
     */
    static ListToTree(list: any[], rootId: number) {
      var tree = [];
      list.find((item) => {
        if (item.parentId == rootId) {
          item.children = this.ListToTree(list, item.id);
          tree.push(item);
        }
      })
      return tree;
    }
  }
}
