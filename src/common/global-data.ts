import { Utils } from "./utils";

export namespace GlobalData {
  export class Str {
    //服务器api请求路径
    private static _APIURL: string = "http://47.107.103.128:10086/";
    //private static _APIURL: string = "   https://localhost:10086/";
    public static get APIURL() {
      return this._APIURL;
    }
  }
  export class Obj {
    //Token
    private static _CURRENT_USER_TOKEN: any;
    public static get CURRENT_USER_TOKEN() {
      this._CURRENT_USER_TOKEN = JSON.parse(
        sessionStorage.getItem("CURRENT_USER_TOKEN")
      );
      return this._CURRENT_USER_TOKEN;
    }

    /**
     * 获取当前管理员
     */
    public static async GetCurrentAdminUser() {
      let url: string = GlobalData.Str.APIURL + "AdminUser/GetCurrentAdminUser";
      let params = {};
      var obj = null;
      await Utils.HttpHelper.Get(url, params).then((res: any) => {
        obj = res;
      });
      return obj;
    }
  }
}
